#!/bin/sh
# converts index.md to html

FILEM=temp.md

# create md file
touch $FILEM
echo "<center><font size=\"1\">" >> $FILEM
echo "Last updated on `date`" >> $FILEM
echo "" >> $FILEM
cat LICENSE.md >> $FILEM
echo "</center></font>" >> $FILEM
echo "" >> $FILEM
cat index.md >> $FILEM

# convert md to html
mkd2html -css bjstyle.css $FILEM

# add header to html for character set
echo "<header> <meta charset="utf-8">  </header>" > header
cat temp.html >> header
mv header index.html

# fix title block and catch any duplicated subs
sed -i 's/<title>/<title>Blackjack Tournament System/g' index.html
sed -i 's/Blackjack Tournament SystemBlackjack Tournament System/Blackjack Tournament System/g' index.html

# cleanup intermediary files
rm temp.*

# completed ok?
echo "Maybe success?"
