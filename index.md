<center><img src="images/blackjack_logo.jpg"></center>
  
<h2 align="center">INTRODUCTION</h2>
Welcome to the Blackjack Tournament System for Kings of War, or BJTSfKoW for short! (Just kidding; we call it Blackjack).  

This system was primarily developed by Ken-in-Yorks and Chris Kellahan. Ken is a (very) busy Tournament Organiser (TO) in Queensland, and CK is an active participant/organiser in the Canberra Kings of War scene. Help has been provided by Matt Croger (The Apron TO) and Tasman Stacey, the TOs behind Clash of Kings Australia (The Southern Hemispheres Premier and largest KoW Tournament).  
  
The main purpose of the Tournament System is to simplify running tournaments for TOs, providing as much relevant material as possible for them, as well as providing material to ease the experience of players. The initial driver for the system was the scoring mechanisms, but the system has developed into so much more since that inception. We felt traditional tournament scoring systems either do not provide enough distinction between matches (W/L/D), or feature swings that are perhaps too wild (20-0 system). In the 20-0 system it is common for very close games that were decided on a single dice roll to end up with scores of 18-2, which is not an accurate reflection of how close the overall game was.  
  
The premise of the Blackjack system is that playing for the scenario win should be a player’s objective, and winning it as decisively as possible, with attrition a secondary, yet still important, factor. Thus, Blackjack attempts to combine win/loss + objective scoring + attrition to arrive at a tournament score that is more representative of the game played.  
  
Blackjack Tournament System currently has 3 components to assist TOs  
  
1. Blackjack Scoring System  
2. Blackjack Player Sheets  
3. Blackjack Maps  
  
####1 THE BLACKJACK SCORING SYSTEM  
  
<a href="https://gitlab.com/blkjck/blackjack-tournament-system/blob/master/resources/BLACKJACK-MASTER-CoK19.xlsx">Blackjack Master Scoring Sheet</a> - this document contains score modifiers for all the current scenarios (updated for CoK19) and for a range of point values (1000pt games through to 2500pt games, just choose the closest relevant matrix). It is not intended for players to need to refer to the full matrix at a tournament. It is intended to assist TOs in preparing their tournament handouts.  
  
#####1.1 HOW TO SCORE USING THE BLACKJACK SYSTEM
  
Scoring a game using this system is extremely straightforward, there are easy steps to get your game’s Tournament Points (TPs)  
  
1. A base score for Win/Loss/Draw  
2. A Victory Point Bonus  
3. An Attrition Bonus  
4. Add these 3 Scores together to get the round TPs  
5. The total Attrition for the Round should also be recorded, as it may be required as a Secondary measure in case of a tie.  
  
Winning or losing is still the most important element of a competitive game, and is weighted accordingly, thus, the base TPs are:  
  
* Win 14  
* Draw 10  
* Loss 7  
   
Victory Point Bonus will be anything from 0 to +4/-4 TPs, to reflect how well the objectives are met. Note the winner will get Bonus Points, whereas their opponent will lose the equivalent points. Achieving the Scenario by the bare minimum will not receive a Bonus.  
  
* Minor Victory		0  
* Victory		+1/-1  
* Solid Victory		+2/-2  
* Major Victory		+3/-3  
* Dominance		+4/-4  
  
Attrition Bonus will be anything from 0 to +3/-3 TPs, to reflect how well one side routed their opponent’s Army. This is achieved by calculating the difference in attrition: Points value of enemy units routed less points value of own units routed, then consult the table for up to +3/-3 TPs. The player with the highest Attrition gets a positive TP Bonus, and their opponent gets an equal negative TP bonus (note: it is not necessarily the winner getting a positive here). These bonuses and penalties will be specific to the size games in play, but the guideline is:  
  
* Too Busy Dancing	0  
* Fisticuffs		+1/-1  
* Bloody Battle		+2/-2  
* Annihilation		+3/+3  
  
A player’s ranking in a tournament, will be based on their TP total. A players total Attrition Points (APs total that is, not the attrition difference) will be used as a tiebreaker.  
  
Twenty-one is the maximum score a player can achieve under this system, hence the name of Blackjack. Scoring Blackjack will be a remarkably difficult achievement, requiring you to have an attrition difference of over 1500pts in a 2000pt game, as well as holding virtually all of the objective points on offer. It is also worth noting that a player who loses the scenario cannot gain more TPs than the player who won the scenario, regardless of how well they do on attrition.  
  
We have tested this system within our internal groups, at casual play nights across Australia and local tournaments, as well as using it to great acclaim at multiple Clash of Kings Australia, 50+ player tournaments in January each year in Canberra at Cancon.  
  
We also welcome any and all community feedback as The Blackjack Team are constantly reviewing feedback, and tinkering with the System, most recently updating to reflect the latest Clash of Kings Supplement.  
  
#####1.2 WHAT BLACKJACK SCORING IS HOPING TO ACHIEVE  
  
Tournament scores should generally end up closer than a 20-0 system, so folk are more likely to have a chance of coming back from a bad round. Narrow wins will typically result in a 14/7 score, instead of the 16/4 result we tend to see currently.  
  
There should be fewer white washes; fewer 20-0s. In fact, to get a 21-0 “Blackjack” (you must shout it out when you get it!) you’ve really got to be going hard. Theoretically we should see fewer people jumping a long way up the ladder in the final round of competition due to crushing an opponent, especially if using swiss draws.  
  
With encouraging playing for the scenario, rather than bare minimum, we hope to reduce the instances of tactical choices such as castling, points denial, plain killing, etc. which are often considered un-fun, yet not completely remove them as viable options for a skilled general in a narrower range of opportunities.  
  
It should hopefully have some impact on army builds. For example, Ken’s Varangur hammer army (an 8 drop army of 4 Mounted Sons + 1 King on Chimera + Herja + 2 mounted mages) could not Blackjack some scenarios, and would be virtually impossible to Blackjack in most others. If a player is aiming to podium at a tournament using this system, they will need to write a list that is capable of not only securing a decisive attrition advantage but also able to secure a decisive objective advantage.  
  
#####1.3 KNOWN CONSIDERATIONS  
  
A tournament organiser will need to be mindful of the following:  
  
1. To ensure a fair availability of tournament points for all players, every table within a given round should use the same number of objectives/tokens as appropriate to the scenario. This may include homogenised points worth of terrain features for Secure across tables.  
2. To ensure a smooth running tournament with targeted handouts for the tables, TOs are encouraged to determine the scenarios and number of objective/token counts before the day. They can be kept secret and only revealed at the beginning of each round. This will avoid needing to prepare multiple lookup tables and scoring variations for each round.  
3. Calculating scores might seem complex at first, but it is really no more complicated than other systems in practice. Just look up the tables and add or subtract the modifiers. Once you have the Tournament Points for the round, and total attrition (still used to determine rank order where TPs are equal), the <a href="https://tabletop.to/">Tabletop TO (TTT)</a> site makes recording them a breeze, and is a highly recommended free resource for tournament organisers.  
  
So we hope everyone who tries it, likes it, as we want the game to be as fair and enjoyable as possible. Thank you for reading this far, and we're very open to hearing your feedback.  
  
####2 BLACKJACK PLAYER SHEETS  
  
We have created <a href="https://gitlab.com/blkjck/blackjack-tournament-system/blob/master/resources/MASTER-PLAYER-SHEET-CoK19.ods">Master Player Sheets</a> for each of the 12 Scenarios as of CoK19. We will be updating again once Kings of War v3 is released. These sheets give all the required information for a given Scenario, including a Map layout to assist visually with the Scenario. There is also a Scoring grid to help players score their round. The sheets are designed so that the TOs can easily select the Scenarios they want, and update the relevant Sections (Red Font), such as Tournament Name, Correct Scoring Matrix, number of Objectives, Start times, etc., and then simply print off as packs for the tables and players ahead of the event.
  
####3 THE BLACKJACK MAPS  
  
We have created maps that can be used for a TO’s event, using Microsoft Powerpoint. These maps give all the required information for terrain, and its effects. The maps are designed so that the TOs can easily update the relevant sections such as Tournament Name and logo, Terrain Heights, etc. Please follow this link to the GitLab repository to download the <a href="https://gitlab.com/blkjck/blackjack-tournament-system/blob/master/resources/blackjackmappack.pptx">Blackjack Map Pack</a>.  
