

Files and instructions for using the Blackjack Tournament System for the tabletop fantasy miniatures wargame Kings of War.

Blackjack is a collaborative effort between several Australian Kings of War Tournament Organisers and enthusiastic fans. It was born out of a desire for tournament scoring to better reflect achieving the objectives of each games' scenario, over traditionally incentivised scoring systems that reward just barely winning on scenario criteria and then killing all your opponents units.

There are several parts to the Blackjack Tournament System. First and foremost is the scoring system itself. To assist in using it are scoring templates, map packs and other utilities.

All the details for how to use these resources, and their rationale, can be found on the <a href="https://blkjck.gitlab.io/blackjack-tournament-system/">Blackjack Tournament System</a> website. Please excuse the basic format, we're migrating our old stuff over to this new site.

Mantic, Kings of War, Kings of War: Vanguard, and all associated characters, names, places and things are TM and (C) Mantic Entertainment Ltd 2019
